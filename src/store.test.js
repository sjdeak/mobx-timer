import timerStore from './store'
import {autorun, toJS} from "mobx";
// const timerStore  = require('./store')
// const autorun = require("mobx").autorun

const counter = function() {
  let cnt = 0

  return () => {
    cnt++
    console.warn(`观察timers 第${cnt}次autorun: `)


    // console.log('timers', toJS(timerStore.timers))

    console.log('timers', toJS(timerStore.timers))

    // const {timers} = timerStore
    // console.log('timers', timers)

    // for (const t of timerStore.timers) {
    //   console.log(t)
    // }
  }
}()

const cancelFn = autorun(counter)

// console.log(timerStore)
// console.log(typeof timerStore.addTimer, timerStore.addTimer)

const t1 = timerStore.addTimer({
  title: 'timer1 60分钟',
  project: 'test project'
}, 60)
t1.start()
setTimeout(() => {
  console.log('10s后停止t1')
  t1.stop()
}, 10000)


const t2 = timerStore.addTimer({
  title: 'timer2 10分钟',
  project: 'test project'
}, 10)

t2.start()
t2.updateDesc({title: '改名后的timer2'})
console.log('t2剩余', t2.durationLeft)
setTimeout(() => {
  console.log('20s后删除t2')
  timerStore.deleteTimer(t2)

  cancelFn()
}, 20000)


