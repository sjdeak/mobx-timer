/*
  eslint-disable react/prefer-stateless-function, react/jsx-boolean-value,
  no-undef, jsx-a11y/label-has-for, react/jsx-first-prop-new-line
*/
import React from 'react'
import { observer, inject } from 'mobx-react'
import helpers from './helpers'
import {toJS} from "mobx";


@inject('store')
class TimersDashboard extends React.Component {

  render() {
    const timers = this.props.store.timers

    return (
      <div className='ui three column centered grid'>
        <div className='column'>
          <EditableTimerList
            timers={timers}
          />
          <ToggleableTimerForm />
        </div>
      </div>
    );
  }
}

class ToggleableTimerForm extends React.Component {
  state = {
    isOpen: false,
  };

  handleFormOpen = () => {
    this.setState({ isOpen: true });
  };

  handleFormClose = () => {
    this.setState({ isOpen: false });
  };

  render() {
    if (this.state.isOpen) {
      return (
        <TimerForm onFormClose={this.handleFormClose} />
      );
    } else {
      return (
        <div className='ui basic content center aligned segment'>
          <button
            className='ui basic button icon'
            onClick={this.handleFormOpen}
          >
            <i className='plus icon' />
          </button>
        </div>
      );
    }
  }
}

@observer
class EditableTimerList extends React.Component {
  render() {
    // console.log('timers: ', this.props.timers)

    // todo deleteTimer时不会被tracked
    const timers = this.props.timers.map((timer) => (
      <EditableTimer
        timer={timer}
        key={timer.id}
      />
    ));
    return (
      <div id='timers'>
        {timers}
      </div>
    );
  }
}

class EditableTimer extends React.Component {
  state = {
    editFormOpen: false,
  };

  handleEditClick = () => {
    this.openForm();
  };

  handleFormClose = () => {
    this.closeForm();
  };

  closeForm = () => {
    this.setState({ editFormOpen: false });
  };

  openForm = () => {
    this.setState({ editFormOpen: true });
  };

  render() {
    if (this.state.editFormOpen) {
      return (
        <TimerForm
          timer={this.props.timer}
          onFormClose={this.handleFormClose}
        />
      );
    } else {
      return (
        <Timer
          timer={this.props.timer}
          onEditClick={this.handleEditClick}
        />
      );
    }
  }
}

@inject('store')
class Timer extends React.Component {
  componentDidMount() {
    this.forceUpdateInterval = setInterval(() => this.forceUpdate(), 50);
  }

  componentWillUnmount() {
    clearInterval(this.forceUpdateInterval);
  }

  handleStartClick = () => {
    this.props.timer.start()
  };

  handleStopClick = () => {
    this.props.timer.stop()
  };

  handleTrashClick = () => {
    this.props.store.deleteTimer(this.timer);
  };

  render() {
    const {timer} = this.props
    const elapsedString = helpers.renderElapsedString(
      timer.elapsed, timer.runningSince
    );
    return (
      <div className='ui centered card'>
        <div className='content'>
          <div className='header'>
            {timer.desc.title}
          </div>
          <div className='meta'>
            {timer.desc.project}
          </div>
          <div className='center aligned description'>
            <h2>
              {elapsedString}
            </h2>
          </div>
          <div className='extra content'>
            <span
              className='right floated edit icon'
              onClick={this.props.onEditClick}
            >
              <i className='edit icon' />
            </span>
            <span
              className='right floated trash icon'
              onClick={this.handleTrashClick}
            >
              <i className='trash icon' />
            </span>
          </div>
        </div>
        <TimerActionButton
          timerIsRunning={!!timer.runningSince}
          onStartClick={this.handleStartClick}
          onStopClick={this.handleStopClick}
        />
      </div>
    );
  }
}

class TimerActionButton extends React.Component {
  render() {
    if (this.props.timerIsRunning) {
      return (
        <div
          className='ui bottom attached red basic button'
          onClick={this.props.onStopClick}
        >
          Stop
        </div>
      );
    } else {
      return (
        <div
          className='ui bottom attached green basic button'
          onClick={this.props.onStartClick}
        >
          Start
        </div>
      );
    }
  }
}

@inject('store')
class TimerForm extends React.Component {
  state = {
    title: this.props.timer ? this.props.timer.title : '',
    project: this.props.timer ? this.props.timer.project : ''
  };

  handleTitleChange = (e) => {
    this.setState({ title: e.target.value });
  };

  handleProjectChange = (e) => {
    this.setState({ project: e.target.value });
  };

  handleSubmit = () => {
    const {timer} = this.props
    const desc = {
      title: this.state.title,
      project: this.state.project,
    }

    if (!timer) {
      this.props.store.addTimer(desc)
    } else {
      timer.updateDesc(desc);
    }
    this.props.onFormClose()
  };

  render() {
    const submitText = this.props.timer ? 'Update' : 'Create';
    return (
      <div className='ui centered card'>
        <div className='content'>
          <div className='ui form'>
            <div className='field'>
              <label>Title</label>
              <input
                type='text'
                value={this.state.title}
                onChange={this.handleTitleChange}
              />
            </div>
            <div className='field'>
              <label>Project</label>
              <input
                type='text'
                value={this.state.project}
                onChange={this.handleProjectChange}
              />
            </div>
            <div className='ui two bottom attached buttons'>
              <button
                className='ui basic blue button'
                onClick={this.handleSubmit}
              >
                {submitText}
              </button>
              <button
                className='ui basic red button'
                onClick={this.props.onFormClose}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TimersDashboard
