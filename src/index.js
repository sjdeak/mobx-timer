import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import {Provider} from "mobx-react";
import {Devtool} from 'mobx-react-devtools'
import store from "./store";


// import './store.test'

// import './play'


ReactDOM.render(
  <Provider store={store}>
  <App />
  </Provider>,
document.getElementById('root')
);
