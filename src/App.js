import React, { Component } from 'react';
import DevTools from 'mobx-react-devtools';
import TimersDashboard from "./TimersDashboard";
import '../semantic-dist/semantic.css'


class App extends Component {
  render() {
    return (
      <div className="App">
        <DevTools/>
        <TimersDashboard/>
      </div>
    );
  }
}

export default App;
