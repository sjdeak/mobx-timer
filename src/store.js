import {action, computed, observable} from 'mobx'
import moment from 'moment'


class TimerStore {
  @observable timers = []

  @action
  addTimer = (desc, duration) => {
    const t = new Timer(desc, duration)
    this.timers.push(t)
    return t
  }

  // MobX observable Array自带的remove方法
  @action
  deleteTimer = (timer) => {
    this.timers.remove(timer)
  }
}

class Timer {
  /**
   *
   * @param desc
   * @param duration  单位是分钟
   */
  constructor(desc, duration=60) {
    this.desc = desc
    this.initialDuration = duration * 60 * 1000
  }

  @observable desc = {
    title: 'title',
    project: 'project'
  }
  @observable elapsed = 0
  @observable initialDuration = 0
  @observable runningSince = null

  @computed
  get durationLeft() {
    const m = moment(this.initialDuration - this.elapsed)
    return m.format('mm分ss秒')
  }

  @action start = () => {
    this.runningSince = moment()
  }
  @action stop = () => {
    const now = moment();
    this.elapsed += now - this.runningSince
    this.runningSince = null
  }

  @action updateDesc = (attrs) => {
    this.desc = Object.assign(this.desc, attrs)
  }
}



export default new TimerStore()
// const ts = new TimerStore()
// ts.addTimer({
//   title: 'timer1 60分钟',
//   project: 'test project'
// }, 60)

