import {computed, autorun, observable, toJS} from "mobx";

let arr = observable.array([])

autorun(() => {
  console.log(arr)
})


arr.push(1)
arr.push(1)
arr.push(1)
arr.push(3)



// let obj = observable({
//   a: 1,
//   b: 2,
//   c: 3
// })
//
// autorun(() => {
//
//   console.log(obj)
// })
//
// obj.a = 2
// obj.b = 3

// let message = observable({
//   title: "Foo",
//   author: {
//     name: "Michel"
//   },
//   likes: [
//     "John", "Sara"
//   ]
// })
//
// autorun(() => {
//   console.log(message.title)
// })
// message.title = "Bar"



// const store = observable({
//   arr: []
// })
//
// autorun(() => {
//   // let l = store.arr.length
//   console.log(store.arr)
// })
//
// store.arr = [1]

// store.arr.push(1)
// store.arr.push(2)
// store.arr.push(3)
// store.arr.pop()
// store.arr.push(4)