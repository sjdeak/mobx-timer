import { observable, action } from 'mobx'
import helpers from '../src/helpers'
import uuid from "uuid";


class Store {
  @observable timers = [
    {
      title: 'Practice squat',
      project: 'Gym Chores',
      id: uuid.v4(),
      elapsed: 5456099,
      runningSince: Date.now(),
      isInRecord: false,
      isInDashboard: true
    },
    {
      title: 'Bake squash',
      project: 'Kitchen Chores',
      id: uuid.v4(),
      elapsed: 1273998,
      runningSince: null,
      isInRecord: false,
      isInDashboard: true
    },
  ]

  @action createTimer = (timer) => {
    const t = helpers.newTimer(timer)
    console.log(t)
    this.timers.push(helpers.newTimer(timer))
  }

  @action updateTimer = (attrs) => {
    this.timers = this.timers.map((timer) => {
      if (timer.id === attrs.id) {
        return Object.assign({}, timer, {
          title: attrs.title,
          project: attrs.project,
        });
      } else {
        return timer;
      }
    })
  };

  @action deleteTimer = (timerId) => {
    this.timers = this.timers.filter(t => t.id !== timerId)
  };

  @action startTimer = (timerId) => {
    const now = Date.now();

    this.timers = this.timers.map((timer) => {
      if (timer.id === timerId) {
        return Object.assign({}, timer, {
          runningSince: now,
        });
      } else {
        return timer;
      }
    })
  }

  @action stopTimer = (timerId) => {
    const now = Date.now();

    this.timers = this.timers.map((timer) => {
      if (timer.id === timerId) {
        const lastElapsed = now - timer.runningSince;
        return Object.assign({}, timer, {
          elapsed: timer.elapsed + lastElapsed,
          runningSince: null,
        });
      } else {
        return timer;
      }
    })
  }
}



export default new Store()